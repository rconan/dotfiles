set encoding=utf-8

set number
set relativenumber

if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

let g:airline_powerline_fonts = 1
let g:airline_solarized_bg='dark'

let g:go_version_warning = 0

" Safely load a project specific RC file
set exrc
set secure
